sass-spec (3.6.3-1.1) unstable; urgency=low

  * Non-maintainer upload (Closes: #955519)
  * Use ruby:any to make the package cross-installable
  * Mark it as multi-arch foreign.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 04 Apr 2020 21:03:34 +0200

sass-spec (3.6.3-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release(s).

  [ Anthony Fok ]
  * Update watch file: Change tag prefix from "v" to "libsass-"
    as per upstream.
  * Bump debhelper dependency to "Build-Depends: debhelper-compat (= 12)".
  * Declare compliance with Debian Policy 4.5.0.
  * Add myself to the list of Uploaders.
  * Update patch 2001.
  * Add dependency on ruby-hrx.
  * Remove unused Lintian override regarding privacy-breaches.

 -- Anthony Fok <foka@debian.org>  Wed, 01 Apr 2020 10:09:03 -0600

sass-spec (3.5.4-2) unstable; urgency=medium

  * Fix Vcs-* URLs.
  * Simplify rules
  * Stop build-depend on dh-buildinfo cdbs.
  * Set Rules-Requires-Root: no.
  * Declare compliance with Debian Policy 4.4.0.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 07 Jul 2019 23:17:36 -0300

sass-spec (3.5.4-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release(s).

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 4.1.4.
  * Fix typo in watch file usage comment.
  * Update Vcs-* fields: Source moved to Salsa.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 25 Jun 2018 20:45:30 +0200

sass-spec (3.5.0-2-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release(s).

  [ Jonas Smedegaard ]
  * Update watch file: Use substitution strings.
  * Advertise DEP-3 format in patch headers.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Drop unneeded disclaimer from GPL-3+ License-Grant.
  * Modernize cdbs:
    + Drop upstream-tarball hints: Use gbp import-orig --uscan.
  * Tighten lintian overrides regarding License-Reference.
  * Modernize Vcs-* fields: Consistently use anonscm hostname.
  * Declare compliance with Debian Policy 4.1.0.
  * Relax cleanup of Ruby, to limit build-depencies sloppily needed
    outside a build chroot.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 11 Sep 2017 16:15:16 +0200

sass-spec (3.4.3-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release(s).

  [ Jonas Smedegaard ]
  * Modernize Vcs-* fields: Use git subdir.
  * Stop override lintian for
    package-needs-versioned-debhelper-build-depends: Fixed in lintian.
  * Update copyright info: Extend coverage of Debian packaging.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 21 Jan 2017 20:19:18 +0100

sass-spec (3.4.0-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release(s).

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize Vcs-* fields:
    + Use https protocol URLs.
    + Use cgit.
  * Update copyright info:
    + Extend coverage of packaging to include current year.
  * Update watch file:
    + Bump to version 4.
    + Use Github pattern.
    + Add usage comment.
  * Modernize git-buildpackage config:
    + Avoid git- prefix.
    + Filter any .git* file.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Update patch 2001.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 11 Dec 2016 11:02:04 +0100

sass-spec (0~20150516-2) unstable; urgency=medium

  * Fix have sass-spec recommend sass-spec-data and ruby-sass.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 25 May 2015 17:00:15 +0200

sass-spec (0~20150516-1) unstable; urgency=low

  * Initial release.
    Closes: bug#779636.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 25 May 2015 14:29:01 +0200
